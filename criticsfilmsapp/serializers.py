from django.db.models import fields
from django.db.models.fields import Field
from rest_framework.serializers import ModelSerializer
from criticsfilmsapp.models import Calificacion, Pelicula

class PeliculaSerializer(ModelSerializer):

    class Meta:
        model = Pelicula
        fields = ('nombre_titulo', 'sinopsis', 'director', 'casting', 'genero', 'año') 
        
class CalificacionSerializer(ModelSerializer):

    class Meta:
        model = Calificacion
        fields = ('nombre_titulo','calificacion', 'promedio_calificacion','critica')