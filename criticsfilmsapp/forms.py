from django import forms
from criticsfilmsapp.models import Calificacion, Pelicula


class peliculaform(forms.ModelForm):

    class Meta:
        model = Pelicula

        fields = [
            'nombre_titulo',
            'sinopsis',
            'director',
            'casting',
            'genero',
            'año',
        ]

        labels = {
            'Titulo': "titulo",
            'Sinopsis': "sinopsis",
            'Director': "director",
            'Casting': "casting",
            'Genero': "genero",
            'Año': "año",
        }

        widgets = {
            'Titulo': forms.TextInput(attrs={'class':'form-control'}),
            'Sinopsis': forms.TextInput(attrs={'class':'form-control'}),
            'Director': forms.TextInput(attrs={'class':'form-control'}),
            'Casting': forms.TextInput(attrs={'class':'form-control'}),
            'Genero': forms.TextInput(attrs={'class':'form-control'}),
            'Año': forms.TextInput(attrs={'class':'form-control'}),
        }
class calificacionform(forms.ModelForm):

    class Meta:
        model = Calificacion

        fields = [
            'nombre_titulo',
            'calificacion',
            'promedio_calificacion',
            'critica',
        ]

        labels = {
            'nombre':'Titulo',
            'calificacion': 'Calificacion',
            'promedio_calificacion': "Promedio calificacion",
            'critica': "Critica",
        }

        widgets = {
            'nombre_titulo': forms.Select(attrs={'class':'form-control'}),
            'Calificacion': forms.TextInput(attrs={'class':'form-control'}),
            'Promedio Calificacion': forms.TextInput(attrs={'class':'form-control'}),
            'Critica': forms.TextInput(attrs={'class':'form-control'}),
        }

