from django.contrib import admin
from django.urls.resolvers import URLPattern
from rest_framework import routers
from .viewset import PeliculaViewSet, CalificacionViewSet
from django.urls import path
from criticsfilmsapp.views import CalificacionCreate, CalificacionDelete, CalificacionUpdate, index, listado, pelicula_view, PeliculaAPI, peliculas_list, pelicula_edit, pelicula_delete, CalificacionList

router = routers.SimpleRouter()
router.register('pelicula', PeliculaViewSet)
router.register('calificacion', CalificacionViewSet)

urlpatterns = router.urls 

"""
urlpatterns = [
    path('', index, name='index'),
    path('nuevo', pelicula_view, name='pelicula_crear'),
    path('listar', peliculas_list, name='pelicula_listar'),
    path('editar/<int:id_pelicula>/', pelicula_edit, name='pelicula_editar'),
    path('eliminar/<int:id_pelicula>/', pelicula_delete, name='pelicula_eliminar'),
    path('listado', listado, name='listado'),
    path('api', PeliculaAPI.as_view(), name='api'),
    path('calificacion/listar', CalificacionList.as_view(), name='calificacion_listar'),
    path('calificacion/nueva', CalificacionCreate.as_view(), name='calificacion_crear'),
    path('calificacion/editar/<int:pk>/', CalificacionUpdate.as_view(), name='calificacion_editar'),
    path('calificacion/eliminar/<int:pk>/', CalificacionDelete.as_view(), name='calificacion_eliminar'),
]"""