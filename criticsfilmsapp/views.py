from django.db.models.fields import PositiveBigIntegerField
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.core import serializers
from django.views.generic import ListView, CreateView
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView, UpdateView 
from criticsfilmsapp.forms import calificacionform, peliculaform
from criticsfilmsapp.models import Calificacion, Pelicula
from rest_framework.views import APIView
import json 

from criticsfilmsapp.serializers import PeliculaSerializer

def listado (request):
    lista = serializers.serialize('json', Pelicula.objects.all())
    return HttpResponse(lista, content_type='application/json')

def index(request):
    return render(request, 'pelicula/index.html')

def pelicula_view(request):
    if request.method == 'POST':
        form = peliculaform(request.POST)
        if form.is_valid():
            form.save()
        return redirect('index')
    else:
        form = peliculaform()

    return render(request, 'pelicula/pelicula_form.html', {'form':form})


def peliculas_list(request):
    pelicula = Pelicula.objects.all().order_by('id')
    contexto = {'peliculas':pelicula}
    return render(request, 'pelicula/pelicula_list.html', contexto)

def pelicula_edit(request, id_pelicula):
    pelicula = Pelicula.objects.get(id=id_pelicula)
    if request.method == 'GET':
        form = peliculaform(instance=pelicula)
    else:
        form = peliculaform(request.POST, instance=pelicula)
        if form.is_valid():
            form.save()
        return redirect('pelicula_listar')
    return render (request, 'pelicula/pelicula_form.html', {'form':form})

def pelicula_delete(request, id_pelicula):
    pelicula = Pelicula.objects.get(id=id_pelicula)
    if request.method == 'POST':
        pelicula.delete()
        return redirect ('pelicula_listar')
    return render (request, 'pelicula/pelicula_delete.html', {'pelicula':pelicula})

class PeliculaAPI(APIView):
    serializer = PeliculaSerializer

    def get(self, request, format=None):
        lista = Pelicula.objects.all()
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data), content_type='application/json')

class CalificacionList(ListView):
    model = Calificacion
    template_name = 'calificacion/calificacion_list.html'

class CalificacionCreate(CreateView):
    model = Calificacion
    template_name = 'calificacion/calificacion_form.html'
    form_class = calificacionform   
    """second_form_class = peliculaform"""
    success_url = reverse_lazy('calificacion_listar')

    """def get_context_data(self, **kwargs):
        context = super(CalificacionCreate, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return context
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            calificacion = form.save()
            calificacion.nombre_titulo = form2.save()
            calificacion.save()
            return HttpResponseRedirect(self.get_success_url())
        else: 
            return self.render_to_response(self.get_context_data(form=form, form2=form2))
    """
class CalificacionUpdate(UpdateView):
    model = Calificacion
    """second_model = Pelicula"""
    template_name = 'calificacion/calificacion_form.html'
    form_class = calificacionform
    """second_form_class = peliculaform"""
    success_url = reverse_lazy('calificacion_listar')
    
    """def get_context_data(self, **kwargs):
        context = super(CalificacionUpdate, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        solicitud = self.model.objects.get(id=pk)
        titulo = self.second_model.objects.get(id=solicitud.id)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=titulo)
        context['id'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id_solicitud = kwargs['pk']
        solicitud =self.model.objects.get(id=id_solicitud)
        calificacion = self.second_model.objects.get(id=solicitud.id)
        form = self.form_class(request.POST, instance = solicitud)
        form2 = self.second_form_class(request.POST, instance = calificacion)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else: 
            return HttpResponseRedirect(self.get_success_url())"""

class CalificacionDelete(DeleteView):
    model = Calificacion
    template_name = 'calificacion/calificacion_delete.html'
    success_url = reverse_lazy('calificacion_listar')


