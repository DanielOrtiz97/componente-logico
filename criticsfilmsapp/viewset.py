from rest_framework import serializers, viewsets

from .models import Calificacion, Pelicula
from .serializers import PeliculaSerializer, CalificacionSerializer

class PeliculaViewSet(viewsets.ModelViewSet):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer

class CalificacionViewSet(viewsets.ModelViewSet):
    queryset = Calificacion.objects.all()
    serializer_class = CalificacionSerializer