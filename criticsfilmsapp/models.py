from django.db import models
from django.db.models.base import Model


class Pelicula(models.Model):
    nombre_titulo = models.CharField(max_length=100)
    sinopsis = models.CharField(max_length=1000)
    director = models.CharField(max_length=50)
    casting = models.CharField(max_length=500)
    genero = models.CharField(max_length=50)
    año = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.nombre_titulo)

class Calificacion(models.Model):
    nombre_titulo = models.ForeignKey(Pelicula, null=True, blank=True, on_delete=models.CASCADE)
    calificacion = models.IntegerField()
    promedio_calificacion = models.FloatField()
    critica = models.CharField(max_length=1000)

