from django.contrib import admin

from criticsfilmsapp.models import Pelicula, Calificacion

admin.site.register(Pelicula)
admin.site.register(Calificacion)
# Register your models here.
