import { createRouter, createWebHistory } from "vue-router";
import Router from 'vue-router'
import ListPelicula from '@/components/ListPelicula';
import EditPelicula from '@/components/EditPelicula';
import DeletePelicula from '@/components/DeletePelicula';
import NewPelicula from '@/components/NewPelicula';
import ListCalificacion from '@/components/ListCalificacion';
import EditCalificacion from '@/components/EditCalificacion';
import DeleteCalificacion from '@/components/DeleteCalificacion';
import NewCalificacion from '@/components/NewCalificacion';
import Home from '@/components/Home';
const routes = [
        {
            path:'/',
            name:'Home',
            props:true,
            component: Home
        },
    
        {   
            path:'/pelicula',
            name:'ListPelicula',
            props:true,
            component: ListPelicula
        },
        {
            path:'/pelicula/:id_pelicula/edit',
            name:'EditPelicula',
            props: true,
            component: EditPelicula
        },
        {
            path:'/pelicula/:id_pelicula/delete',
            name:'DeletePelicula',
            props: true,
            component: DeletePelicula  
        },
        {
            path:'/pelicula/new',
            name:'NewPelicula',
            props: true,
            component: NewPelicula  
        },
        {
            path:'/pelicula/calificacion',
            name:'ListCalificacion',
            props: true,
            component: ListCalificacion 
        },
        {
            path:'/pelicula/calificacion/:id/edit',
            name:'EditCalificacion',
            props: true,
            component: EditCalificacion 
        },
        {
            path:'/pelicula/calificacion/:id/delete',
            name:'DeleteCalificacion',
            props: true,
            component: DeleteCalificacion  
        },
        {
            path:'/pelicula/calificacion/new',
            name:'NewCalificacion',
            props: true,
            component: NewCalificacion 
        },
    ];
const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;





